<!--
SPDX-FileCopyrightText: 2021-2022 embeach

SPDX-License-Identifier: MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later

Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/pcrlt-licenses.git@master#README.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-licenses.git@master#README.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding README.md

Note-PCRLT:MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"
-->

# PCRLT Licenses
The repository contains PCRLT annotations for some widely used licenses and legal/normative files.  
The goal is to allow other repositories to reference this repository for PCRLT annotation of such widely used files to avoid repetitive work. Have a look at the repository <https://gitlab.com/emb_std/spdx-utils> to see how such references can be specified.

## License
The Project is SPDXv2.3[^spdx], REUSEv3.0[^reuse] and PCRLTv0.4[^pcrlt] compliant - i.e. copyright and license information are provided on a per-file basis. A summary is provided in LICENSE.spdx as SPDX document. Full license texts can be found in the LICENSES folder.  

The repository contains references to the following licenses: 0BSD, Apache-2.0, CC0-1.0, CC-BY-4.0, CC-BY-ND-4.0, CC-BY-SA-4.0, GPL-3.0-or-later, MIT  
Some of the normative files are licensed under some kind of CC-BY-ND-4.0 license. But they can be part of open source projects, because they are legal companion files, which are normally not considered as normal content.  
The other files have multiple open source licenses options (i.e. multi-licensed).  
The SPDX PackageLicenseConcluded as well as PackageLicenseDeclared is: CC-BY-ND-4.0 AND MIT  
Note: This is not an independent normative statement, but instead is the package license based on the license information of the individual files.  
See LICENSE.spdx for further details.

[^spdx]: SPDXv2.3: <https://spdx.github.io/spdx-spec/>
[^reuse]: REUSEv3.0: <https://reuse.software/spec/>
[^pcrlt]: PCRLTv0.4: <https://gitlab.com/emb_std/pcrlt>
